/**
 * Exchanger v1.0 - A VanillaJS widget for converting currency
 * Written By - Joel Parsons
 * July 24, 2017
 */

(function () {

	/**
	 * Configuration
	 * Each widget included in your HTML must have its id attribute registered in the widget_ids array
	 * Available options for currency conversion are listed in the options array, consult https://fixer.io for additional options 
	 */

	var widget_ids = ['exchanger-widget-0','exchanger-widget-1','exchanger-widget-2'],
		options = ['CAD','USD','EUR'];

	/**
	 * Utility Functions
	 * inArray()
	 */

	function inArray(needle, haystack) {

	    var length = haystack.length;

	    for (var i = 0; i < length; i++) {
	        if (haystack[i] == needle)
	        	return true;
	    }
	    
	    return false;

	}


	/**
	 * Exchanger - Singleton object that handles all ajax requests and data processing
	 * @constructor
	 */

	function Exchanger() {

		apiLink = 'https://api.fixer.io/latest?base=';
			
	};

	/** validate() - Object method to process the data from a widget and validate it before submitting to the API */

	Exchanger.prototype.validate = function (widget_id) {

		Exchanger.call(this);

		/* Get elements from the DOM */
		var widget = document.getElementById(widget_id),
			toggles = widget.getElementsByTagName('select'),
			inputs = widget.getElementsByTagName('input');

		/* Validate currency options against options array, and verify amount is a valid number */
		if (inArray(toggles[0].value, options) && inArray(toggles[1].value, options) && !isNaN(inputs[0].value)) {

			if (toggles[0].value == toggles[1].value) // If converting to the same currency type...
				inputs[1].value = parseFloat(inputs[0].value).toFixed(2); // Duplicate the value from amount without submitting ajax.
			else
				exchanger.convert(toggles[0].value, toggles[1].value, inputs[0].value, inputs[1]); // Otherwise, submit the request.

		}
		else {

			/* Error handling */
			alert('Something went wrong! Please refresh the page and try again.')
			console.log('Error: invalid currency (only ' + options.toString() + ' are supported)')
		
		}

	}


	/** convert() - Object method to submit GET requests to the API and display the processed results */

	Exchanger.prototype.convert = function (base, convertTo, amount, result) {

		Exchanger.call(this);
		
		/* Open connection for AJAX requests */
		var xhr = new XMLHttpRequest();
		xhr.open('GET', apiLink + base);
		xhr.send(null);

		xhr.onreadystatechange = function () {

			var DONE = 4;
			var OK = 200;

			if (xhr.readyState === DONE) {

				if (xhr.status === OK) { // Once data has been successfully sent back from the server...

					data = JSON.parse(xhr.responseText); // Process the data into JSON...
					result.value = parseFloat(data.rates[convertTo] * amount).toFixed(2); // And display the value in the result input field.
				
				}
				else {

					/* Error handling */
					alert('Exchanger Widget encountered an error connecting to the currency database. Check back later to see if service has been restored.');
					console.log('Error: ' + xhr.status);

				}

			}

		};

	};


	/**
	 * Widget - References an HTML object that contains all the UI elements of the widget
	 * @constructor
	 * @param {string} id - The id attribute of the <div> element that will act as the parent container for the widget
	 */

	function Widget(id) {

		/* Register the child elements as properties to be created in the DOM */
		el = document.getElementById(id),
		title = document.createElement('h3'),
		amountLabel = document.createElement('label'),
		amountInput = document.createElement('input'),
		baseToggle = document.createElement('select'),
		resultLabel = document.createElement('label'),
		resultInput = document.createElement('input'),
		convertToToggle = document.createElement('select');

		/* Widget Title Text */
		title.innerHTML = 'Currency Converter';
	    el.appendChild(title);

	    /* Amount Input Label */
		amountLabel.innerHTML = 'Type in amount and select currency:';
        el.appendChild(amountLabel);

        /* Amount Input with Event Listener */
		amountInput.type = 'number';
		amountInput.name = 'baseAmount';
		amountInput.min = '0.00';
		amountInput.step = 'any';
		amountInput.value = '0.00';
		amountInput.addEventListener('change', function () {
			exchanger.validate(this.parentElement.id);
		});
        el.appendChild(amountInput);

        /* Base Currency Select Box with Event Listener */
        baseToggle.name = 'baseCur';
        baseToggle.addEventListener('change', function () {
			exchanger.validate(this.parentElement.id);
		});
        el.appendChild(baseToggle);

        /* Result Input Label */
        resultLabel.innerHTML = 'Converted amount:';
        el.appendChild(resultLabel);

        /* Result Input (disabled to prevent user input, for display purposes only) */
		resultInput.type = 'text';
		resultInput.name = 'resultAmount';
		resultInput.disabled = true;
        el.appendChild(resultInput);

		/* Convert To Currency Select Box with Event Listener */
        convertToToggle = document.createElement('select');
        convertToToggle.name = 'convertToCur';
        convertToToggle.addEventListener('change', function () {
			exchanger.validate(this.parentElement.id);
		});
        el.appendChild(convertToToggle);

        // Create an option for each currecy type in each select box
		for (var j = 0; j < options.length; j++) {
		    var option = document.createElement('option');
		    option.value = options[j];
		    option.text = options[j];
		    baseToggle.appendChild(option);
		    var option = document.createElement('option');
		    option.value = options[j];
		    option.text = options[j];
		    convertToToggle.appendChild(option);
		}

        /* Select the second option for the Convert To Select Box */
		convertToToggle.children[1].selected = true;

		/* Disclaimer Link */
		disclaimer = document.createElement('a');
		disclaimer.href = '#';
		disclaimer.innerHTML = 'Disclaimer'
		disclaimer.onclick = function () {
			alert('Legal Disclaimer -- Currency calculations performed using real time information from https://fixer.io -- Exchanger Widget \u00a9 Joel Parsons, 2017.')
		};
		el.appendChild(disclaimer);
	
	};


	/**
	 * Object Instantiation
	 * The singleton Exchanger object is created here, as well as a Widget object for each id referenced in the widget_ids array
	 */

	var exchanger = new Exchanger();

	for (var k = 0; k < widget_ids.length; k++) {
		new Widget(widget_ids[k]);
	}

})();