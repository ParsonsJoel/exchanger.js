# Exchanger.js #

### A simple VanillaJS widget for converting one currency into another ###

* Supports multiple currencies
* Lightweight and simple to use
* Customizable
* Reusable
* Compatible with Bootstrap 4 (Responsive)
* SASS support
* Version 1.0

### How do I get set up? ###

* Place link tag for exchanger.css in the head of your HTML
* Place script tag for exchanger.js at the end of the body of your HTML
* Widget may be used multiple times on the same page, each needs a div with a unique id
* The id for each widget must be placed in the widget_ids array in the Configuration section of exchanger.js
* See example.html for a demo of how to use the widget in a responsive grid layout using Bootstrap 4
* To enable SASS editing of the widget, run the following command from the project directory command line: sass --watch scss/exchanger.scss:css/exchanger.css